package mainAssignment;

public class Laptop {
	
	//instance variables
	private int laptopID;
	private String laptopType;
	private int quantity;
	private String softwareType;
	private int numOnLoan;
	private int numOfTimesLoaned;
	
	//constructor
	public Laptop (int laptopID, String laptopType, int quantity, String softwareType, int numOnLoan, int numOfTimesLoaned){
		this.laptopID=laptopID;
		this.laptopType=laptopType;
		this.quantity=quantity;
		this.softwareType=softwareType;
		this.numOnLoan=numOnLoan;
		this.numOfTimesLoaned=numOfTimesLoaned;
	}
	//Accessor-getter
			public int getlaptopID(){
				return laptopID;
			}
			
			public String getlaptopType(){
				return laptopType;
			}
			
			public int getQuantity(){
				return quantity;
			}
			
			public String getsoftwareType(){
				return softwareType;
			}
			
			public int getnumOnLoan(){
				return numOnLoan;
			}
			
			public int getNumOfTimesLoaned(){
				return numOfTimesLoaned;
			}
			
	//Mutator-setter
			public void setlaptopID(int laptopID){
				this.laptopID=laptopID;
			}
			public void setlaptopType(String laptopType){
				this.laptopType=laptopType;
			}
			
			public void setQuantity(int quantity){
				this.quantity=quantity;
			}
			
		     public void setsoftwareType(String softwareType){
					this.softwareType=softwareType;
			}
			
		     public void setnumOnLoan(int numOnLoan){
					this.numOnLoan=numOnLoan;
			}
			
			
			public void setNumTimesLoaned(int numTimesLoaned){
					this.numOfTimesLoaned=numTimesLoaned;
			}
	 

}

