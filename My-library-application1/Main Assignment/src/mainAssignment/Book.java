package mainAssignment;


public class Book {
	
	//instance variables
	    private int bookID;
		private String title;
		private String author;
		private String genre;
		private int quantity;
		private int numOnLoan;
		private int numOfTimesLoaned;
		
	//constructor
		public Book (int bookID, String title, String author, String genre, int quantity, int numOnLoan, int numOfTimesLoaned){
			this.bookID=bookID;
			this.title=title;
			this.author=author;
			this.genre=genre;
			this.quantity=quantity;
			this.numOnLoan=numOnLoan;
			this.numOfTimesLoaned=numOfTimesLoaned;
}
	
		//Accessor-getter
		public int getBookID(){
			return bookID;
		}
		
		public String getTitle(){
			return title;
		}
		
		public String getAuthor(){
			return author;
		}
		
		public String getGenre(){
			return genre;
		}
		
		public int getQuantity(){
			return quantity;
		}
		
		public int getNumOnLoan(){
			return numOnLoan;
		}
		
		public int getNumOfTimesLoaned(){
			return numOfTimesLoaned;
		}
		
	//Mutator-setter
		public void setBookID(int bookID){
			this.bookID=bookID;
		}
		public void setTitle(String title){
			this.title=title;
		}
		
	     public void setAuthor(String author){
			this.author=author;
		}
	     
	     public void setGenre (String genre){
	    	 this.genre=genre;
	     }
	     
	     public void setQuantity(int quantity){
	    	 this.quantity=quantity;
	     }
		
	     public void setNumOnLoan(int numOnLoan){
				this.numOnLoan=numOnLoan;
		}
		
		
		public void setNumOfTimesLoaned(int numOfTimesLoaned){
				this.numOfTimesLoaned=numOfTimesLoaned;
		}
	
		
		
		
		
		
	//optional method "print details" (prints all in one)NEED TO CREATE BOOK IN TESTER FIRST FOR THIS TO WORK.
		/*public void printBookDetails(){
			System.out.println("Book Details");
			System.out.println("Title = " + getTitle());
			System.out.println("Author = " +getAuthor());
			System.out.println("Number on Loan = " + getNumOnLoan());
			System.out.println("Num Of Times Loaned " + getNumOfTimesLoaned());
		}*/
	
		}
		
		

