package mainAssignment;

import java.util.ArrayList;
import java.util.Scanner;




public class Library {
	Scanner sc = new Scanner(System.in);
	
	//instance variables
	private String libraryName;
	private String address;
	private String telephoneNumber;
	private String openingHours;
	private ArrayList<Book> LibraryOne = new ArrayList<Book>();
	private ArrayList<Laptop>LibraryTwo = new ArrayList<Laptop>();
	
	//constructor
	public Library(String libraryName, String address, String telephoneNumber, String openingHours, 
			ArrayList<Book> LibraryOne, ArrayList<Laptop>LibraryTwo){
		this.libraryName = libraryName;
		this.address = address;
		this.telephoneNumber = telephoneNumber;
		this.openingHours = openingHours;
		this.LibraryOne = LibraryOne;
		this.LibraryTwo = LibraryTwo;
		}
	
	//Accessors-Getters
	public String getlibraryName(){
		return libraryName;
	}
	
	public String getaddress(){
		return address;
	}
	
	public String gettelephoneNumber(){
		return telephoneNumber;
	}
	
	public String getopeningHours(){
		return openingHours;
	}
	
	public ArrayList<Book>getLibraryOne(){
		return LibraryOne;
	}
	
	public ArrayList<Laptop>getLibraryTwo(){
		return LibraryTwo;
	}
	
	//Mutators-Setters
	public void setlibraryName(String libraryName){
		this.libraryName = libraryName;
	}
	
	public void setaddress(String address){
		this.address = address;
	}
	
	public void settelephoneNumber(String telephoneNumber){
		this.telephoneNumber = telephoneNumber;
	}
	
	public void setopeningHours (String openingHours){
		this.openingHours = openingHours;
	}
	
	public void setLibraryOne(ArrayList<Book>LibraryOne){
		this.LibraryOne = LibraryOne;
	}
	
	public void setLibraryTwo(ArrayList<Laptop>LibraryTwo){
		this.LibraryTwo = LibraryTwo;
	}
	
	//login details method
	//An attempt at an additional feature. if/else statement attempted as loop would not work for me.
	//Placed into case within tester to show functionality.
	public void loginDetails(){
			Scanner input = new Scanner(System.in);
		   

		    String username;
		    String password;

		    System.out.println("Enter Login Details:");
		    
		    System.out.println("username: ");
		    username = input.nextLine();
		    System.out.println("password: ");
		    password = input.nextLine();

		    //users check = new users(username, password);
		    if(password.equals("password") && username.equals("david") ){
		        System.out.println("Logged in");
		    }
		    else{
		        System.out.println("Try again");
		        System.out.println("username: ");
			    username = input.nextLine();
			    System.out.println("password: ");
			    password = input.nextLine();

			    //users check = new users(username, password);
			    if(password.equals("password") && username.equals("david") ){
			        System.out.println("Logged in");
			    }
			    else{
			    	System.out.println("Failed");
			    	
			    }    
		    }
	}
	
	//print Library details
		public void printLibraryDetails(){
			System.out.println("Library Name: " + this.libraryName);
			System.out.println("Address: " + this.address);
			System.out.println("Telephone Number: " + this.telephoneNumber);
			System.out.println("Opening Hours: " + this.openingHours);
		}
	
		//Methods relating to Book class and Helper Utilities
	
	//print Book details
	public void printBookDetails(){
		for(int i=0; i< this.LibraryOne.size(); i++){
			System.out.println("\nBook ID: " + this.LibraryOne.get(i).getBookID());
			System.out.println("Book title: " + this.LibraryOne.get(i).getTitle());
			System.out.println("Author: " + this.LibraryOne.get(i).getAuthor());
			System.out.println("Genre: " + this.LibraryOne.get(i).getGenre());
			System.out.println("Quantity: " + this.LibraryOne.get(i).getQuantity());
			System.out.println("Number On Loan: " + this.LibraryOne.get(i).getNumOnLoan());
			System.out.println("Number Of Times Loaned: " + this.LibraryOne.get(i).getNumOfTimesLoaned());
		}
	}
	
	
	//add book method
		public void addBook(ArrayList<Book>LibraryOne){
		System.out.print("Please enter a book ID: ");
		int bookID = sc.nextInt();
		sc.nextLine();
		System.out.print("Please enter a book title: ");
		String title = sc.nextLine();
		System.out.print("Please enter an Author: ");
		String author = sc.nextLine();
		System.out.println("Please enter a genre: ");
		String genre = sc.nextLine();
		System.out.print("Please enter the quantity of books: ");
		int quantity = sc.nextInt();
		sc.nextLine();
		System.out.print("Please enter the quantity on loan: ");
		int numOnLoan = sc.nextInt();
		sc.nextLine();
		System.out.print("Please enter the number of times the book has been loaned out: ");
		int numOfTimesLoaned = sc.nextInt();
		sc.nextLine();
		Book newBook = new Book(bookID, title, author, genre, quantity, numOnLoan, numOfTimesLoaned);
		this.LibraryOne.add(newBook);
		}
	
	//edit book method
		public void editBook(ArrayList<Book>LibraryOne){
			System.out.println("Please select which feature of the book you would like to edit");
			System.out.println("1: The book ID");
			System.out.println("2: The books Title");
			System.out.println("3: The books Author");
			System.out.println("4: The books genre");
			System.out.println("5: The books Quantity");
			System.out.println("6: The Quantity of books On Loan");
			System.out.println("7: The number of times the book has been loaned");
			switch (sc.nextInt()){
			
			case 1: editBookID(LibraryOne);
				break;
				
			case 2: editBookTitle(LibraryOne);
				break;
				
			case 3: editBookAuthor(LibraryOne);
				break;
				
			case 4: editBookGenre(LibraryOne);
				break;
			        
			case 5: editBookQuantity(LibraryOne);
				break;
				
			case 6: editBookQuantityOnLoan(LibraryOne);
				break;
				
			case 7:editNumOfTimesLoaned(LibraryOne);
				break;
			}
		}
		
		//individual components for the edit menu
		
			//Edit book ID option
			public void editBookID(ArrayList<Book>LibraryOne){
			System.out.println("Please select which Book ID you would like to edit: 0-6");
			int editedBook = sc.nextInt();
			sc.nextLine();
			System.out.println("Please enter the new Book ID");
			int newBookID = sc.nextInt();
			sc.nextLine();
			LibraryOne.get(editedBook).setBookID(newBookID);
			}
		
	        //edit book title option
		    public void editBookTitle(ArrayList<Book>LibraryOne){
			System.out.println("Please select which Book Title you would like to edit: 0-6");
			int editedBook = sc.nextInt();
			sc.nextLine();
			System.out.println("Please enter the books new title");
			String newTitle = sc.nextLine();
			LibraryOne.get(editedBook).setTitle(newTitle);
		    }
			
			//edit the book author
			public void editBookAuthor(ArrayList<Book>LibraryOne){
			System.out.println("Please select which Author you would like to edit: 0-6");
			int editedBook = sc.nextInt();
			sc.nextLine();
			System.out.println("Please enter the new Author name");
			String newAuthorName = sc.nextLine();
			LibraryOne.get(editedBook).setAuthor(newAuthorName);
			}
			
			//edit the book genre
			public void editBookGenre(ArrayList<Book>LibraryOne){
			System.out.println("Please select which book you would like to editthe genre for: 0-6");
			int editedBook = sc.nextInt();
			sc.nextLine();
			System.out.println("Please enter the new genre");
			String newGenre = sc.nextLine();
			LibraryOne.get(editedBook).setGenre(newGenre);
			}
			
			//edit the book quantity
			public void editBookQuantity(ArrayList<Book>LibraryOne){
			System.out.println("Please Select which quantity of books you would like to edit: 0-6");
			int editedBook = sc.nextInt();
			sc.nextLine();
			System.out.println("Please enter the new quantity of books");
			int newBookQuantity = sc.nextInt();
			sc.nextLine();
			LibraryOne.get(editedBook).setQuantity(newBookQuantity);
			}
			
			//edit the quantity of books on loan
			public void editBookQuantityOnLoan(ArrayList<Book>LibraryOne){
			System.out.println("Please Select which book you would like to edit the quantity for: 0-6");
			int editedBook = sc.nextInt();
			sc.nextLine();
			System.out.println("Please enter the new quantity of books on Loan");
			int newBookQuantityOnLoan = sc.nextInt();
			sc.nextLine();
			LibraryOne.get(editedBook).setNumOnLoan(newBookQuantityOnLoan);
			}
			
			//edit number of times loaned
			public void editNumOfTimesLoaned(ArrayList<Book>LibraryOne){
			System.out.println("Please select the book you would like to change the number of times it has been loaned: 0-6");
			int editedBook = sc.nextInt();
			sc.nextLine();
			System.out.println("Please enter the new amount the book has been onLoan");
			int newNumOfTimesLoaned = sc.nextInt();
			sc.nextLine();
			LibraryOne.get(editedBook).setNumOfTimesLoaned(newNumOfTimesLoaned);
			}

			//loan method
		public void loanBook(ArrayList<Book>LibraryOne) {
			System.out.println("please select the book you want to loan 0-6");
			int loanBookOption = sc.nextInt();
				if (LibraryOne.get(loanBookOption).getNumOnLoan() < LibraryOne.get(loanBookOption).getQuantity()) {
				LibraryOne.get(loanBookOption).setNumOnLoan(LibraryOne.get(loanBookOption).getNumOnLoan()+ 1);
				LibraryOne.get(loanBookOption).setNumOfTimesLoaned(LibraryOne.get(loanBookOption).getNumOfTimesLoaned() + 1);
					System.out.println( "The book is now on loan");
				} else if(LibraryOne.get(loanBookOption).getQuantity()<LibraryOne.get(loanBookOption).getNumOnLoan()){
					System.out.println("This book is not available as all copies are currently on loan");
				}
		}
		
		    //return method
		public void returnBook(ArrayList<Book>LibraryOne){
			System.out.println("Please select the book you want to return 0-6");
			int returnBookOption = sc.nextInt();
			if(LibraryOne.get(returnBookOption).getNumOnLoan()> 0){
				LibraryOne.get(returnBookOption).setNumOnLoan(LibraryOne.get(returnBookOption).getNumOnLoan() - 1);
			}else if(LibraryOne.get(returnBookOption).getNumOnLoan()<=0){
				System.out.println("The books have already been returned");
				
			}
		}
			
	   //delete book method
		public void deleteBook(ArrayList<Book>LibraryOne){
			System.out.println("please enter the book you would like to delete by ID 0-6");
			int deleteBookOption = sc.nextInt();
			this.LibraryOne.remove(deleteBookOption);
		}
		
		
		//METHODS RELATING TO LAPTOP AND LAPTOP UTILITIES
		
				//print Laptop details
				public void printLaptopDetails(){
					for(int i=0; i< this.LibraryTwo.size(); i++){
						System.out.println("\nLaptop ID: " + this.LibraryTwo.get(i).getlaptopID());
						System.out.println("Laptop Type: " + this.LibraryTwo.get(i).getlaptopType());
						System.out.println("Quantity: " + this.LibraryTwo.get(i).getQuantity());
						System.out.println("Software Type: " + this.LibraryTwo.get(i).getsoftwareType());
						System.out.println("Number On Loan: " + this.LibraryTwo.get(i).getnumOnLoan());
						System.out.println("Number Of Times Loaned: " + this.LibraryTwo.get(i).getNumOfTimesLoaned());
					}
				}
				
			
				//add Laptop method
				public void addLaptop(ArrayList<Laptop>LibraryTwo){
				System.out.print("Please enter a Laptop ID: ");
				int laptopID = sc.nextInt();
				sc.nextLine();
				System.out.print("Please enter the type of laptop: ");
				String laptopType = sc.nextLine();
				System.out.print("Please enter the quantity of laptops: ");
				int quantity = sc.nextInt();
				sc.nextLine();
				System.out.println("Please enter the type of software");
				String softwareType = sc.nextLine();
				System.out.print("Please enter the quantity on loan: ");
				int numOnLoan = sc.nextInt();
				sc.nextLine();
				System.out.print("Please enter the number of times the Laptop has been loaned out: ");
				int numOfTimesLoaned = sc.nextInt();
				sc.nextLine();
				Laptop newLaptop = new Laptop(laptopID, laptopType, quantity, softwareType, numOnLoan, numOfTimesLoaned);
				this.LibraryTwo.add(newLaptop);
				}
				
				//edit laptop method
						public void editLaptop(ArrayList<Laptop>LibraryTwo){
							System.out.println("Please select which feature of the laptop you would like to edit");
							System.out.println("1: The laptop ID");
							System.out.println("2: The laptop Type");
							System.out.println("3: The laptop Quantity");
							System.out.println("4: the software Type");
							System.out.println("5: The Quantity of laptops On Loan");
							System.out.println("6: The number of times the laptop has been loaned");
							switch (sc.nextInt()){
							
							case 1: editLaptopID(LibraryTwo);
								break;
								
							case 2: editLaptopType(LibraryTwo);
								break;
								
							case 3: editLaptopQuantity(LibraryTwo);
								break;
								
							case 4: editsoftwareType(LibraryTwo);
								break;
							        
							case 5: editLaptopQuantityOnLoan(LibraryTwo);
								break;
								
							case 6: editnumOfTimesLoaned(LibraryTwo);
								break;
							}
						}
						
						//individual components for the edit menu
						
							//Edit laptop ID option
							public void editLaptopID(ArrayList<Laptop>LibraryTwo){
							System.out.println("Please select which laptop ID you would like to edit: 0-2");
							int editedLaptop = sc.nextInt();
							sc.nextLine();
							System.out.println("Please enter the new laptop ID");
							int newLaptopID = sc.nextInt();
							sc.nextLine();
							LibraryTwo.get(editedLaptop).setlaptopID(newLaptopID);
							}
						
					        //edit laptop type
						    public void editLaptopType(ArrayList<Laptop>LibraryTwo){
							System.out.println("Please select which laptop type you would like to edit: 0-2");
							int editedLaptop = sc.nextInt();
							sc.nextLine();
							System.out.println("Please enter the new laptop Type");
							String newLaptopType = sc.nextLine();
							LibraryTwo.get(editedLaptop).setlaptopType(newLaptopType);
						    }
							
							//edit the laptop quantity
							public void editLaptopQuantity(ArrayList<Laptop>LibraryTwo){
							System.out.println("Please Select which quantity of laptops you would like to edit: 0-2");
							int editedLaptop = sc.nextInt();
							sc.nextLine();
							System.out.println("Please enter the new quantity of laptops");
							int newLaptopQuantity = sc.nextInt();
							sc.nextLine();
							LibraryTwo.get(editedLaptop).setQuantity(newLaptopQuantity);
							}
							
							//edit the software Type
							public void editsoftwareType(ArrayList<Laptop>LibraryTwo){
							System.out.println("Please Select which software type you would like to edit: 0-2");
							int editedLaptop = sc.nextInt();
							sc.nextLine();
							System.out.println("Please enter the new software type");
							String newLaptopsoftwareType = sc.nextLine();
							sc.nextLine();
							LibraryTwo.get(editedLaptop).setsoftwareType(newLaptopsoftwareType);
							}
							
							//edit the quantity of laptops on loan
							public void editLaptopQuantityOnLoan(ArrayList<Laptop>LibraryTwo){
							System.out.println("Please Select which laptop you would like to edit the quantity for: 0-2");
							int editedLaptop = sc.nextInt();
							sc.nextLine();
							System.out.println("Please enter the new quantity of laptops on Loan");
							int newLaptopQuantityOnLoan = sc.nextInt();
							sc.nextLine();
							LibraryTwo.get(editedLaptop).setnumOnLoan(newLaptopQuantityOnLoan);
							}
							
							//edit number of times loaned
							public void editnumOfTimesLoaned(ArrayList<Laptop>LibraryTwo){
							System.out.println("Please select the laptop you would like to change the number of times it has been loaned: 0-2");
							int editedLaptop = sc.nextInt();
							sc.nextLine();
							System.out.println("Please enter the new amount the laptop has been onLoan");
							int newNumOfTimesLoaned = sc.nextInt();
							sc.nextLine();
							LibraryTwo.get(editedLaptop).setNumTimesLoaned(newNumOfTimesLoaned);
							}
				
					
							
							//loan method
						public void loanLaptop(ArrayList<Laptop>LibraryTwo) {
							System.out.println("please select the laptop you want to loan 0-2");
							int loanLaptopOption = sc.nextInt();
								if (LibraryTwo.get(loanLaptopOption).getnumOnLoan() < LibraryTwo.get(loanLaptopOption).getQuantity()) {
								LibraryTwo.get(loanLaptopOption).setnumOnLoan(LibraryTwo.get(loanLaptopOption).getnumOnLoan()+ 1);
								LibraryTwo.get(loanLaptopOption).setNumTimesLoaned(LibraryTwo.get(loanLaptopOption).getNumOfTimesLoaned() + 1);
									System.out.println( "The laptop is now on loan");
								} else if(LibraryTwo.get(loanLaptopOption).getQuantity()<LibraryTwo.get(loanLaptopOption).getnumOnLoan()){
									System.out.println("This laptop is not available as all devices are currently on loan");
								}
						}
						
						    //return method
						public void returnLaptop(ArrayList<Laptop>LibraryTwo){
							System.out.println("Please select the laptop you want to return 0-2");
							int returnLaptopOption = sc.nextInt();
							if(LibraryTwo.get(returnLaptopOption).getnumOnLoan()> 0){
								LibraryTwo.get(returnLaptopOption).setnumOnLoan(LibraryTwo.get(returnLaptopOption).getnumOnLoan() - 1);
							}else if(LibraryTwo.get(returnLaptopOption).getnumOnLoan()<=0){
								System.out.println("The laptops have already been returned");
								
							}
						}
						
					   //delete laptop method
						public void deleteLaptop(ArrayList<Laptop>LibraryTwo){
							System.out.println("please enter the laptop you would like to delete by ID 0-2");
							int deleteLaptopOption = sc.nextInt();
							this.LibraryTwo.remove(deleteLaptopOption);
						}

		
		
		
}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
		

	
	
	
	


 
	
	



