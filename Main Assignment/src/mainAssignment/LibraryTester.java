package mainAssignment;

//This code fully functions with the way it is set up.
//validation attempted alongside an additonal feature of login details



import java.util.ArrayList;
import java.util.Scanner;


public class LibraryTester {
	
	public static void main(String[] args) {
	Scanner sc = new Scanner(System.in);
	
	//boolean value used to allow exit program function to work. Set to false so that menu remains open until changed
	boolean exitProgram = false;
	
	//create ArrayList of books
	ArrayList<Book> mybooks = new ArrayList<Book>();
	mybooks = HelperUtilities.generateBooks();
	
	
	//create ArrayList of laptops
	ArrayList<Laptop> mylaptops = new ArrayList<Laptop>();
	mylaptops = LaptopUtilities.generateLaptops();
	
	
	//create a Library
	Library Library = new Library("Davids Library", "123 Malone Road", "07865456776", 
			"Monday-Friday = 9am-5pm, Sat-Sun = 12pm-6pm", mybooks, mylaptops);
	
	//Switch set up for menu operation
	//A while loop is set up within the switch so that after an option is complete the menu will reappear.
	
	while (!exitProgram)
	{
	int userOption = printMenu(sc);
	
	switch (userOption){
	
	case 1:
		Library.loginDetails();
	break;
	
	case 2:
		Library.printLibraryDetails();
	break;
		
	case 3:
		System.out.println("List of Current Books");
		Library.printBookDetails();
	break;
	
	case 4:
		Library.addBook(mybooks);
		Library.printBookDetails();
		break;
		
	case 5:
		Library.editBook(mybooks);
		Library.printBookDetails();
		break;
		
	case 6:
		Library.deleteBook(mybooks);
        Library.printBookDetails();
		break;
		
	case 7:
		Library.loanBook(mybooks);
		Library.printBookDetails();
		break;
	
	case 8: 
		Library.returnBook(mybooks);
	    Library.printBookDetails();
	     break;
	     
	case 9:
		Library.printLaptopDetails();
		break;
	
	case 10: 
		Library.addLaptop(mylaptops);
		Library.printLaptopDetails();
		break;
		
	case 11:
		Library.editLaptop(mylaptops);
		Library.printLaptopDetails();
		break;
		
	case 12:
		Library.deleteLaptop(mylaptops);
		Library.printLaptopDetails();
		break;
		
	case 13:
		Library.loanLaptop(mylaptops);
		Library.printLaptopDetails();
		break;
		
	case 14: 
		Library.returnLaptop(mylaptops);
		Library.printLaptopDetails();
		break;
		
	case 15:
		exitProgram = true;
    break;
		
	}
	}
	}
	
	
	//Main Menu
	//The menu contains validation for user input. The user must input an appropraite value (1-8) otherwise they 
	// will be prompted to enter another value. 
	

	public static int printMenu(Scanner sc){
	System.out.println("1: Enter login details");
	System.out.println("2: Print the Library Details");
	System.out.println("3: Print the Book Details");
	System.out.println("4: Add a new Book");
	System.out.println("5: Edit a Books Details");
	System.out.println("6: Delete Book");
	System.out.println("7: Loan Book");
	System.out.println("8: Return Book");
	System.out.println("9: Print Laptop Details");
	System.out.println("10: Add a new Laptop");
	System.out.println("11: Edit a laptops Details");
	System.out.println("12: Delete Laptop");
	System.out.println("13: Loan Laptop");
	System.out.println("14: Return Laptop");
	System.out.println("15: Exit Programme");
	int userOption = validateInputInt(sc,"Please enter an option");
	return userOption;
	
}
	public static int validateInputInt(Scanner sc, String infoText) {
		boolean validInput = false;
		int userInput = 0;
		do {
			System.out.println(infoText);
			if (sc.hasNextInt()) {
				userInput = sc.nextInt();
				sc.nextLine();
				if (userInput > 0 && userInput < 16) {
					validInput = true;
				} else {
					System.out.print("Input invalid: ");
				}
			} else {
				System.out.print("Input invalid: ");
				sc.nextLine();
			}
		} while (!validInput);
		return userInput;
	}
} 


		

	
	
				