package mainAssignment;

import java.util.ArrayList;
public class HelperUtilities {
	
	private static int[] bookID = {
			001,002,003,004,005,006,007
	};
	
	private static String[] title = {
			"Harry Potter and the Philosopher's Stone","Harry Potter and the Chamber of Secrets",
			"Harry Potter and the Prisoner of Azkaban","Harry Potter and the Goblet of Fire",
			"Harry Potter and the Order of the Phoenix","Harry Potter and the Half-Blood Prince",
			"Harry Potter and the Deathly Hallows"
    };
	
	private static String[] author = {
			"J.K. Rowling","J.K. Rowling","J.K. Rowling","J.K. Rowling","J.K. Rowling","J.K. Rowling","J.K. Rowling"
	};
	
	private static String[] genre = {
			"Fiction", "Fiction", "Fiction", "Fiction", "Fiction", "Fiction", "Fiction"
	};
	
	private static int[] quantity = {
			10,10,10,10,10,10,10
	};
	
	private static int[] numOnLoan = {
			0,0,0,0,0,0,0
	};
	
	private static int[] numOfTimesLoaned = {
			0,0,0,0,0,0,0
	};
	
	public static ArrayList<Book> generateBooks(){
		ArrayList<Book> mybooks = new ArrayList<Book>();
		for (int i = 0; i<bookID.length; i++)
		{
			mybooks.add(new Book(bookID[i], title[i], author[i], genre[i], quantity[i], numOnLoan[i], numOfTimesLoaned[i]));
		};
		
		return mybooks;
		
}
/*	public static Book[] generateBooksArray()
	{
		Book[] books = new Book[bookID.length];
		
		for (int i = 0; i < books.length; i++) {
			books[i] = new Book(bookID[i],title[i], author[i], quantity[i], numOnLoan[i], numOfTimesLoaned[i]);
		}
		
		return books;
		
	}*/
}



