package mainAssignment;

import java.util.ArrayList;


public class LaptopUtilities {
	
private static int[] laptopID = {
		001,002,003
};

private static String[] laptopType = {
		"Dell", "HP", "MacBook"
};

private static int[] quantity = {
		10,10,10
};

private static String[] softwareType = {
		"Microsoft", "Microsoft", "OSX"
};

private static int[] numOnLoan = {
		0,0,0
};

private static int[] numOfTimesLoaned = {
		0,0,0
};

public static ArrayList<Laptop> generateLaptops(){
	ArrayList<Laptop> mylaptops = new ArrayList<Laptop>();
	for (int i = 0; i<laptopID.length; i++)
	{
		mylaptops.add(new Laptop(laptopID[i], laptopType[i], quantity[i], softwareType[i], numOnLoan[i], numOfTimesLoaned[i]));
	};
	
	return mylaptops;



}
}
